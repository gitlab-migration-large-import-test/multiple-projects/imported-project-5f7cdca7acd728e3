# frozen_string_literal: true

module AuthenticationSetup
  def self.included(base)
    base.class_eval do
      before do
        next if AppConfig.anonymous_access

        error!("Missing basic authorization", 401) unless headers["Authorization"]

        AuthHelper.authenticate_basic_auth_base64!(headers["Authorization"])
      rescue AuthHelper::AuthError
        error!("Unauthorized", 401)
      end
    end
  end
end
