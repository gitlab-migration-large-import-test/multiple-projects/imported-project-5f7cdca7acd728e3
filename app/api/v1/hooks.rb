# frozen_string_literal: true

module V1
  # @deprecated
  #
  # This API version is deprecated and will be removed in the next major release
  class Hooks < Grape::API
    helpers do
      # Authenticate request against gitlab token
      #
      # @return [void]
      def authenticate!
        gitlab_auth_token = CredentialsConfig.gitlab_auth_token
        return unless gitlab_auth_token

        gitlab_token = headers["X-Gitlab-Token"] || ""
        return if ActiveSupport::SecurityUtils.secure_compare(gitlab_token, gitlab_auth_token)

        error!("Invalid gitlab authentication token", 401)
      end

      # Initialize Gitlab client with project access token
      #
      # @return [void]
      def init_gitlab!
        Gitlab::ClientWithRetry.client_access_token = project.gitlab_access_token
      end

      # Is event supported
      #
      # @return [Boolean]
      def event_supported?
        %w[push merge_request pipeline note issue work_item].include?(params[:object_kind])
      end

      # Full project path
      #
      # @return [String]
      def project_name
        params.dig(:project, :path_with_namespace)
      end

      def project
        Project.find_by(name: project_name)
      end

      # Handle push hook event
      #
      # @return [void]
      def push
        Webhooks::PushEventHandler.call(project_name: project_name, commits: params[:commits])
      end

      # Handle merge_request hook event
      #
      # @return [void]
      def merge_request
        return unless %w[close merge reopen approved].include?(params.dig(:object_attributes, :action))

        Webhooks::MergeRequestEventHandler.call(
          project_name: project_name,
          mr_iid: params.dig(:object_attributes, :iid),
          action: params.dig(:object_attributes, :action),
          merge_status: params.dig(:object_attributes, :merge_status)
        )
      end

      # Handle comment hook event
      #
      # @return [void]
      def note
        Webhooks::CommentEventHandler.call(
          project_name: project_name,
          mr_iid: params.dig(:merge_request, :iid),
          discussion_id: params.dig(:object_attributes, :discussion_id),
          note: params.dig(:object_attributes, :note)
        )
      end

      # Handle pipeline hook event
      #
      # @return [void]
      def pipeline
        Webhooks::PipelineEventHandler.call(
          project_name: project_name,
          source: params.dig(:object_attributes, :source),
          status: params.dig(:object_attributes, :status),
          mr_iid: params.dig(:merge_request, :iid),
          merge_status: params.dig(:merge_request, :merge_status),
          source_project_id: params.dig(:merge_request, :source_project_id),
          target_project_id: params.dig(:merge_request, :target_project_id)
        )
      end

      # Handle issue hook event
      #
      # @return [void]
      def issue
        return unless params.dig(:object_attributes, :action) == "close"

        Webhooks::IssueEventHandler.call(
          project_name: project_name,
          issue_iid: params.dig(:object_attributes, :iid)
        )
      end

      # Skip work_item type issue hook events
      #
      # @return [void]
      def work_item; end
    end

    params do
      requires :object_kind, type: String, desc: "Webhook event type"
      requires :project, type: Hash do
        requires :path_with_namespace, type: String, desc: "Project path with namespace"
      end

      # Push event attributes
      optional :commits, type: Array do
        requires :added, type: Array
        requires :modified, type: Array
        requires :removed, type: Array
      end
      # Object attributes hash can contain different kinds of attributes depending on object kind
      optional :object_attributes, type: Hash do
        # Merge request\issue attributes
        optional :iid, type: Integer, desc: "Merge request or issue iid"
        optional :action, type: String, desc: "Merge request or issue action"
        optional :merge_status, type: String, desc: "Merge request merge status"
        # Comment attributes
        optional :note, type: String, desc: "Comment text"
        optional :discussion_id, type: String, desc: "Comment discussion id"
        # Pipeline attributes
        optional :status, type: String, desc: "Pipeline status"
        optional :source, types: [String, Hash], desc: "Merge request source branch or pipeline source"
      end
      optional :merge_request, type: Hash do
        requires :iid, type: Integer, desc: "Merge request iid"
        # only present in pipeline webhhoks
        optional :merge_status, type: String, desc: "Merge request merge status"
        optional :source_project_id, type: Integer, desc: "Merge request source project id"
        optional :target_project_id, type: Integer, desc: "Merge request target project id"
      end
    end

    before { authenticate! }
    after_validation { init_gitlab! }

    desc "Process gitlab webhook"
    post :hooks do
      return unless event_supported?

      send(params[:object_kind])
    end
  end
end
