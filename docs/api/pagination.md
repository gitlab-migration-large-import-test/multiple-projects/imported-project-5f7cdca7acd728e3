# Pagination

Sometimes, the returned result spans many pages. When listing resources, you can pass the following parameters:

| Parameter | Description |
| --------- | ----------- |
| `page`    | The page number to return (default: `1`) |
| `per_page`| The number of items to return per page (default: `10`, max: `100`) |
| `offset`  | The number of items to skip (default: `0`) |

In the following example, we request the second page of projects with 25 projects per page:

```sh
curl "https://example.com/api/v2/projects?page=2&per_page=25"
```
